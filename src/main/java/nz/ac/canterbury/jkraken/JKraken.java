/* ************************************************************************
 *
 * University of Canterbury
 * __________________
 *
 *  [2013] - [2019] University of Canterbury
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of University of Canterbury and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to University of Canterbury
 * and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from  the University of Canterbury.
 */
package nz.ac.canterbury.jkraken;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author davidm
 */
public class JKraken {

    private final Auth auth;
    private boolean debug;
    private final OkHttpClient httpClient;

    public JKraken(String key, String secret, boolean debug) {
        auth = new Auth();
        auth.APIKey = key;
        auth.APISecret = secret;
        this.debug = debug;
        httpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .followRedirects(false)
                .build();
    }

    private JKrakenResult request(String json, File file) throws JKrakenException {
        Response response = null;
        try {
            if (debug) {
                System.out.println("DEBUG::JSON Request: " + json);
            }
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("upload", file.getName(), RequestBody.create(MediaType.parse("application/octet-stream"), file))
                    .addFormDataPart("data", json)
                    .build();

            Request request = new Request.Builder()
                    .url("https://api.kraken.io/v1/upload")
                    .post(requestBody)
                    .build();

            Call call = httpClient.newCall(request);
            response = call.execute();
            String strResponse = response.body().string();

            if (debug) {
                System.out.println("DEBUG::JSON Response: " + strResponse);
            }
            return new JKrakenResult(httpClient, new JSONObject(strResponse));
        } catch (JKrakenException | JSONException | IOException ex) {
            throw new JKrakenException(ex.getMessage(), ex);
        } finally {
            if(response != null) {
                response.close();
            }
        }
    }

    public JKrakenResult upload(Options opts) throws JKrakenException {
        if (opts.getFile() == null) {
            throw new JKrakenException("File parameter was not provided");
        }
        File file = new File(opts.getFile());
        if (!file.exists()) {
            throw new JKrakenException("File " + file.getAbsolutePath() + " does not exist");
        }
        try {
            JSONObject data = new JSONObject();
            data.put("wait", opts.isWait());
            data.put("auth", auth.toJSON());
            data.put("webp", opts.isWebp());
            data.put("lossy", opts.isLossy());

            return request(data.toString(), file);
        } catch (JSONException ex) {
            throw new JKrakenException(ex.getMessage(), ex);
        }
    }

    private class Auth {

        String APIKey;
        String APISecret;

        JSONObject toJSON() throws JSONException {
            JSONObject j = new JSONObject();
            j.put("api_key", APIKey);
            j.put("api_secret", APISecret);
            return j;
        }
    }

    public static class Options {

        private String file;
        private boolean wait = true;
        private boolean webp = false;
        private boolean lossy = true;


        String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        boolean isWait() {
            return wait;
        }

        public void setWait(boolean wait) {
            this.wait = wait;
        }

        boolean isWebp() {
            return webp;
        }

        public void setWebp(boolean webp) {
            this.webp = webp;
        }

        boolean isLossy() {
            return lossy;
        }

        public void setLossy(boolean lossy) {
            this.lossy = lossy;
        }
    }
}
