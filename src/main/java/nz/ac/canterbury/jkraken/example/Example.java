/* ************************************************************************
 *
 * University of Canterbury
 * __________________
 *
 *  [2013] - [2019] University of Canterbury
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of University of Canterbury and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to University of Canterbury
 * and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from  the University of Canterbury.
 */
package nz.ac.canterbury.jkraken.example;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import nz.ac.canterbury.jkraken.JKraken;
import nz.ac.canterbury.jkraken.JKrakenException;
import nz.ac.canterbury.jkraken.JKrakenResult;

/**
 * Example using the features of the API Library.
 *
 */
public class Example {

    public static void main(String[] args) {
        new Example().run();
    }
    private void run() {
        JKraken kraken = new JKraken("<key>", "<secret>", true);
        JKraken.Options options = new JKraken.Options();
        options.setLossy(true);
        options.setWebp(false);
        options.setFile("/path/to/input.jpg");
        options.setWait(true);

        try {
            JKrakenResult result =  kraken.upload(options);

            System.out.println("Message: "+result.getMessage());
            System.out.println("Filename: "+result.getFilename());
            System.out.println("ID: "+result.getId());
            System.out.println("Kraked size: "+result.getKrakedSize());
            System.out.println("Original size: "+result.getOriginalSize());
            System.out.println("Saved bytes: "+result.getSavedBytes());
            writeOptimizedImageToFile(result.getKrakedFile(), new File("/path/to/output.jpg"));

            result.close();

        } catch (JKrakenException | IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private void writeOptimizedImageToFile(InputStream is, File file) throws IOException {
        try (FileOutputStream os = new FileOutputStream(file)) {
            //Write optimized file over original upload file
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        }
    }
}
