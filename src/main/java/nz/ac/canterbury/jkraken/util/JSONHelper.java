/* ************************************************************************
 *
 * University of Canterbury
 * __________________
 *
 *  [2013] - [2019] University of Canterbury
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of University of Canterbury and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to University of Canterbury
 * and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from  the University of Canterbury.
 */
package nz.ac.canterbury.jkraken.util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Wrapper around some common calls on the JSONOject
 * @author davidm
 */
public class JSONHelper {

    /**
     *
     * @param o the json object
     * @param field the field to get
     * @param defaultValue the default value if the field is empty or not found
     * @return the string or the default if not found
     * @throws JSONException the exception
     */
    public static String getStringSafe(JSONObject o, String field, String defaultValue) throws JSONException {
        if (o.isNull(field)) {
            return defaultValue;
        } else {
            return o.getString(field);
        }
    }

    /**
     *
     * @param o the json object
     * @param field the field to get
     * @param defaultValue the default value if the field is empty or not found
     * @return the long or the default if not found
     * @throws NumberFormatException the exception
     * @throws JSONException the exception
     */
    public static long getLongSafe(JSONObject o, String field, long defaultValue) throws NumberFormatException, JSONException {
        if (o.isNull(field)) {
            return defaultValue;
        } else {
            return o.getLong(field);
        }
    }
}
