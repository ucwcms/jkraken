/*
 * ***********************************************************************
 *
 * University of Canterbury __________________
 *
 * [2013] - [2019] University of Canterbury All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of
 * University of Canterbury and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to University of
 * Canterbury and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from the
 * University of Canterbury.
 */
package nz.ac.canterbury.jkraken;

public class JKrakenException extends Exception {
    private String extra;

    public JKrakenException() {
        super();
    }

    public JKrakenException(String message) {
        super(message);
    }

    public JKrakenException(String message, Throwable cause) {
        super(message, cause);
    }

    public JKrakenException(String message, Exception ex, String extra) {
        super(message, ex);
        this.extra = extra;
    }

    public JKrakenException(String message, String extra) {
        super(message);
        this.extra = extra;
    }

    public String getExtra() {
        return extra;
    }


}
