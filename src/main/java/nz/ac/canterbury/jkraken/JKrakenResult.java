/* ************************************************************************
 *
 * University of Canterbury
 * __________________
 *
 *  [2013] - [2019] University of Canterbury
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of University of Canterbury and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to University of Canterbury
 * and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from  the University of Canterbury.
 */
package nz.ac.canterbury.jkraken;

import java.io.IOException;
import java.io.InputStream;

import nz.ac.canterbury.jkraken.util.JSONHelper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author davidm
 */
public final class JKrakenResult {

    private String filename;
    private long originalSize;
    private long krakedSize;
    private long savedBytes;
    private String krakedUrl;
    private String id;
    private InputStream krakedFile;
    private String message;

    public JKrakenResult(OkHttpClient httpClient, JSONObject j) throws JKrakenException {
        try {
            filename = JSONHelper.getStringSafe(j, "file_name", "");
            id = JSONHelper.getStringSafe(j, "id", "");
            krakedSize = JSONHelper.getLongSafe(j, "kraked_size", 0L);
            if (j.has("kraked_url")) {
                krakedUrl = JSONHelper.getStringSafe(j, "kraked_url", "").replace(" ", "%20");
            }
            originalSize = JSONHelper.getLongSafe(j, "original_size", 0L);
            savedBytes = JSONHelper.getLongSafe(j, "saved_bytes", 0L);
            boolean success = j.getBoolean("success");
            message = JSONHelper.getStringSafe(j, "message", "");

            if (success) {
                krakedFile = grabRawStream(httpClient, getKrakedUrl());
                if(krakedFile == null) {
                    throw new JKrakenException(message, j.toString());
                }
            } else {
                throw new JKrakenException(message, j.toString());
            }
        } catch (JSONException | NumberFormatException | IOException ex) {
            throw new JKrakenException(ex.getMessage(), ex, j.toString());
        }
    }

    private InputStream grabRawStream(OkHttpClient client, String url) throws IOException {
        if (url != null && !url.isEmpty()) {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            return client.newCall(request).execute().body().byteStream();
        }
        return null;
    }

    /**
     * Get the input stream of the kraked file or call close() on this object
     * @return the inputstream THIS MUST BE CLOSED
     */
    public InputStream getKrakedFile() {
        return krakedFile;
    }

    public String getMessage() {
        return message;
    }

    public String getFilename() {
        return filename;
    }

    public long getOriginalSize() {
        return originalSize;
    }

    public long getKrakedSize() {
        return krakedSize;
    }

    public long getSavedBytes() {
        return savedBytes;
    }

    private String getKrakedUrl() {
        return krakedUrl;
    }

    public String getId() {
        return id;
    }

    public void close() throws IOException {
        if(krakedFile != null) {
            krakedFile.close();
        }
    }

}
