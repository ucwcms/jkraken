# jkraken

Java Library for uploading images to Kraken.io image optimizer. Only supports image upload from local filesystem.

## Usage

```java
JKraken kraken = new JKraken("<key>", "<secret>");
JKraken.Options options = new JKraken.Options();
options.setLossy(true);
options.setWebp(false);
options.setFile("/path/to/upload/image.jpg");

JKrakenResult result =  kraken.upload(options);
InputStream optimizedImage = result.getKrakedFile();
```

## Build

`$ mvn clean install`


